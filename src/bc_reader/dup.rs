use super::BcReader;
use crate::{common::*, util::*};

use bevy::prelude::*;
use dup_crypto::keys::{ed25519::PublicKey as Pubkey, PublicKey};
use indicatif::ProgressBar;
use num_traits::Zero;
use std::path::PathBuf;

fn default_blockchain_path() -> UsablePath {
	UsablePath(
		directories::BaseDirs::new().map_or_else(PathBuf::new, |base_dirs| {
			base_dirs
				.config_dir()
				.to_path_buf()
				.join("duniter/duniter_default/g1")
		}),
	)
}

#[derive(Clone, Debug, clap::Args)]
pub struct BcOpt {
	/// Duniter directory
	#[clap(short='p', long, default_value_t=default_blockchain_path())]
	pub bc_path: UsablePath,

	/// Only import this number of blocks
	#[clap(short, long)]
	pub blocks: Option<usize>,
}

#[derive(Clone, Debug, Default)]
pub struct Config {}

impl blockchain::Config for Config {
	type Pubkey = Pubkey;
	type BlockNumber = u32;
}

#[derive(Clone, Resource)]
pub struct BcDup {
	pub blocks: Vec<blockchain::Block<Config>>,
	pub state: state::State<Config>,
}

impl BcReader<Config> for BcDup {
	fn available_blocks(&self) -> u32 {
		self.blocks.len() as u32
	}
	fn current_block(&self) -> Option<&blockchain::Block<Config>> {
		if self.state.next_block_number.is_zero() {
			None
		} else {
			self.blocks.get(self.state.next_block_number as usize - 1)
		}
	}
	fn get_block(&self, number: u32) -> Option<&blockchain::Block<Config>> {
		self.blocks.get(number as usize)
	}
	fn state(&self) -> &state::State<Config> {
		&self.state
	}
	fn state_mut(&mut self) -> &mut state::State<Config> {
		&mut self.state
	}
	fn apply_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self.blocks.get(self.state.next_block_number as usize) {
			return Some(self.state.apply_block(block));
		}
		None
	}
	fn revert_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self.blocks.get(self.state.next_block_number as usize - 1) {
			return Some(self.state.revert_block(block));
		}
		None
	}
}

impl BcDup {
	pub fn new(opt: &BcOpt) -> Self {
		let mut nb_blocks = std::fs::read_dir(&opt.bc_path).unwrap().count() * 250;
		if let Some(blocks_limit) = opt.blocks {
			nb_blocks = nb_blocks.min(blocks_limit);
		}
		let bar = ProgressBar::new(nb_blocks as u64);
		let mut blocks = Vec::new();

		'chunks: for chunk_nb in 0usize.. {
			match std::fs::File::open(opt.bc_path.0.join(format!("chunk_{}-250.json", chunk_nb))) {
				Ok(chunk) => {
					if let Ok(chunk) = simd_json::from_reader::<_, serde_json::Value>(chunk) {
						for block in chunk["blocks"].as_array().unwrap() {
							blocks.push(blockchain::Block {
								certs: block["certifications"]
									.as_array()
									.unwrap()
									.iter()
									.map(|cert| parse_cert(cert.as_str().unwrap()))
									.collect(),
								excluded: block["excluded"]
									.as_array()
									.unwrap()
									.iter()
									.map(|pubkey| {
										PublicKey::from_base58(pubkey.as_str().unwrap()).unwrap()
									})
									.collect(),
								hash: parse_hash(block["hash"].as_str().unwrap()),
								idties: block["identities"]
									.as_array()
									.unwrap()
									.iter()
									.map(|idty| parse_idty(idty.as_str().unwrap()))
									.collect(),
								number: block["number"].as_u64().unwrap() as u32,
								median_time: block["medianTime"].as_u64().unwrap(),
							});
							if blocks.len() >= nb_blocks {
								break 'chunks;
							}
						}
					}
				}
				Err(_) => break,
			}
			bar.inc(250);
		}
		bar.finish();

		Self {
			blocks,
			state: state::State::empty(),
		}
	}
}

pub fn parse_idty(s: &str) -> blockchain::Idty<Config> {
	let mut iter = s.split(':');
	blockchain::Idty {
		pubkey: Pubkey::from_base58(iter.next().unwrap()).unwrap(),
		block_number: iter
			.nth(1)
			.unwrap()
			.split_once('-')
			.unwrap()
			.0
			.parse()
			.unwrap(),
		username: iter.next().unwrap().to_string(),
	}
}

pub fn parse_cert(s: &str) -> blockchain::Cert<Config> {
	let mut iter = s.split(':');
	blockchain::Cert {
		issuer: Pubkey::from_base58(iter.next().unwrap()).unwrap(),
		receiver: Pubkey::from_base58(iter.next().unwrap()).unwrap(),
		blockstamp: iter.next().unwrap().parse().unwrap(),
	}
}

pub fn parse_hash(s: &str) -> [u8; 32] {
	let mut r = [0; 32];
	hex::decode_to_slice(s, &mut r).unwrap();
	r
}
