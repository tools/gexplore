use crate::{blockchain, state};

use num_traits::Zero;
use rand::Rng;
use subxt::backend::rpc::RpcClient;

#[subxt::subxt(
	runtime_metadata_path = "res/gdev.scale",
	derive_for_all_types = "Debug"
)]
pub mod runtime {}

pub type Client = subxt::OnlineClient<Runtime>;
pub type AccountId = subxt::utils::AccountId32;
pub type IdtyId = u32;
pub type BlockNumber = u32;
pub type TxProgress = subxt::tx::TxProgress<Runtime, Client>;
pub type Balance = u64;
pub type AccountData =
	runtime::runtime_types::pallet_duniter_account::types::AccountData<Balance, IdtyId>;
pub type AccountInfo = runtime::runtime_types::frame_system::AccountInfo<u32, AccountData>;
pub type Hash = sp_core::H256;

pub enum Runtime {}
impl subxt::config::Config for Runtime {
	type AssetId = ();
	type Hash = Hash;
	type AccountId = AccountId;
	type Address = sp_runtime::MultiAddress<Self::AccountId, u32>;
	type Signature = sp_runtime::MultiSignature;
	type Hasher = subxt::config::substrate::BlakeTwo256;
	type Header = subxt::config::substrate::SubstrateHeader<BlockNumber, Self::Hasher>;
	type ExtrinsicParams = subxt::config::DefaultExtrinsicParams<Self>;
}

#[derive(Clone, Debug, Default)]
pub struct Config {
	pub rpc: String,
}

impl blockchain::Config for Config {
	type Pubkey = u32;
	type BlockNumber = u32;
}

#[derive(Clone, bevy::ecs::system::Resource)]
pub struct BcSubstrate {
	pub blockchain: blockchain::Blockchain<Config>,
	pub state: state::State<Config>,
	pub rpc_client: RpcClient,
	pub client: Client,
	pub config: Config,
}

impl blockchain::BcReader<Config> for BcSubstrate {
	fn available_blocks(&self) -> u32 {
		self.blockchain.blocks.len() as u32
	}
	fn current_block(&self) -> Option<&blockchain::Block<Config>> {
		if self.state.next_block_number.is_zero() {
			None
		} else {
			self.blockchain
				.blocks
				.get(self.state.next_block_number as usize - 1)
		}
	}
	fn get_block(&self, number: u32) -> Option<&blockchain::Block<Config>> {
		self.blockchain.blocks.get(number as usize)
	}
	fn state(&self) -> &state::State<Config> {
		&self.state
	}
	fn state_mut(&mut self) -> &mut state::State<Config> {
		&mut self.state
	}
	fn apply_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self
			.blockchain
			.blocks
			.get(self.state.next_block_number as usize)
		{
			return Some(self.state.apply_block(block));
		}
		None
	}
	fn revert_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self
			.blockchain
			.blocks
			.get(self.state.next_block_number as usize - 1)
		{
			return Some(self.state.revert_block(block));
		}
		None
	}
}

impl BcSubstrate {
	pub async fn new(config: Config) -> Self {
		let rpc_client = RpcClient::from_url(&config.rpc).await.unwrap();
		Self {
			blockchain: blockchain::Blockchain { blocks: Vec::new() },
			client: Client::from_rpc_client(rpc_client.clone()).await.unwrap(),
			rpc_client,
			config,
			state: state::State::empty(),
		}
	}
	pub fn _apply_to(&mut self, block_number: u32) {
		for block in self
			.blockchain
			.blocks
			.iter()
			.skip(self.state.next_block_number.saturating_sub(1) as usize)
			.take(block_number.saturating_sub(self.state.next_block_number) as usize)
		{
			self.state.apply_block(block);
		}
	}
	pub fn _revert_to(&mut self, block_number: u32) {
		for block in self
			.blockchain
			.blocks
			.iter()
			.skip(block_number as usize)
			.take(self.state.next_block_number.saturating_sub(block_number) as usize)
		{
			self.state.revert_block(block);
		}
	}
	pub fn _go_to(&mut self, block_number: u32) {
		if block_number >= self.state.next_block_number {
			self._apply_to(block_number);
		} else {
			self._revert_to(block_number);
		}
	}
	pub fn _go_to_latest(&mut self) {
		for block in self
			.blockchain
			.blocks
			.iter()
			.skip(self.state.next_block_number.saturating_sub(1) as usize)
		{
			self.state.apply_block(block);
		}
	}
}
