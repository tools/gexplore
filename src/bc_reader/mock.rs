use super::BcReader;
use crate::common::*;

use num_traits::Zero;
use rand::Rng;

#[derive(Clone, Debug, Default)]
pub struct Config {}

impl blockchain::Config for Config {
	type Pubkey = u32;
	type BlockNumber = u32;
}

#[derive(Clone)]
pub struct BcMock {
	pub blocks: Vec<blockchain::Block<Config>>,
	pub state: state::State<Config>,
}

impl BcReader<Config> for BcMock {
	fn available_blocks(&self) -> u32 {
		self.blocks.len() as u32
	}
	fn current_block(&self) -> Option<&blockchain::Block<Config>> {
		if self.state.next_block_number.is_zero() {
			None
		} else {
			self.blocks.get(self.state.next_block_number as usize - 1)
		}
	}
	fn get_block(&self, number: u32) -> Option<&blockchain::Block<Config>> {
		self.blocks.get(number as usize)
	}
	fn state(&self) -> &state::State<Config> {
		&self.state
	}
	fn state_mut(&mut self) -> &mut state::State<Config> {
		&mut self.state
	}
	fn apply_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self.blocks.get(self.state.next_block_number as usize) {
			return Some(self.state.apply_block(block));
		}
		None
	}
	fn revert_one(&mut self) -> Option<state::Changeset<Config>> {
		if let Some(block) = self.blocks.get(self.state.next_block_number as usize - 1) {
			return Some(self.state.revert_block(block));
		}
		None
	}
}

impl BcMock {
	pub fn _new(blocks: usize) -> Self {
		let mut rng = rand::thread_rng();

		Self {
			blocks: (0..blocks)
				.map(|number| blockchain::Block {
					number: number as u32,
					median_time: number as u64 * 600,
					certs: {
						if number > 1 {
							let rand_n = rng.gen_range(0..number as u32);
							vec![
								blockchain::Cert {
									blockstamp: number as u32,
									issuer: rand_n,
									receiver: number as u32,
								},
								blockchain::Cert {
									blockstamp: number as u32,
									issuer: (rand_n + rng.gen_range(1..number as u32))
										% number as u32,
									receiver: number as u32,
								},
							]
						} else if number > 0 {
							vec![blockchain::Cert {
								blockstamp: number as u32,
								issuer: 1,
								receiver: number as u32,
							}]
						} else {
							Vec::new()
						}
					},
					idties: vec![blockchain::Idty {
						block_number: number as u32,
						pubkey: number as <Config as blockchain::Config>::Pubkey,
						username: number.to_string(),
					}],
					excluded: Vec::new(),
					hash: [0; 32],
				})
				.collect(),
			state: state::State::empty(),
		}
	}
	pub fn _apply_to(&mut self, block_number: u32) {
		for block in self
			.blocks
			.iter()
			.skip(self.state.next_block_number.saturating_sub(1) as usize)
			.take(block_number.saturating_sub(self.state.next_block_number) as usize)
		{
			self.state.apply_block(block);
		}
	}
	pub fn _revert_to(&mut self, block_number: u32) {
		for block in self
			.blocks
			.iter()
			.skip(block_number as usize)
			.take(self.state.next_block_number.saturating_sub(block_number) as usize)
		{
			self.state.revert_block(block);
		}
	}
	pub fn _go_to(&mut self, block_number: u32) {
		if block_number >= self.state.next_block_number {
			self._apply_to(block_number);
		} else {
			self._revert_to(block_number);
		}
	}
	pub fn _go_to_latest(&mut self) {
		for block in self
			.blocks
			.iter()
			.skip(self.state.next_block_number.saturating_sub(1) as usize)
		{
			self.state.apply_block(block);
		}
	}
}
