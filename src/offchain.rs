use crate::common::blockchain;

use bevy::prelude::*;
use dashmap::DashMap;

#[derive(Clone, Default)]
pub struct CesiumplusAccount {
	pub title: Option<String>,
	pub location_pos: Option<(f64, f64)>,
	pub location_title: Option<String>,
	pub avatar: Option<Vec<u8>>,
}

#[derive(Default, Resource)]
pub struct OffchainData<C: blockchain::Config> {
	cesiumplus_accounts: DashMap<C::Pubkey, CesiumplusAccount>,
}

impl<C: blockchain::Config> OffchainData<C> {
	pub fn get_cesiumplus_account(
		&mut self,
		pubkey: C::Pubkey,
	) -> dashmap::mapref::one::Ref<C::Pubkey, CesiumplusAccount> {
		match self.cesiumplus_accounts.entry(pubkey) {
			dashmap::mapref::entry::Entry::Occupied(entry) => entry.into_ref().downgrade(),
			dashmap::mapref::entry::Entry::Vacant(entry) => entry
				.insert(fetch_cesiumplus_account::<C>(pubkey).unwrap_or_default())
				.downgrade(),
		}
	}
}

fn fetch_cesiumplus_account<C: blockchain::Config>(pubkey: C::Pubkey) -> Option<CesiumplusAccount> {
	let mut resp =
		reqwest::blocking::get(format!("https://g1.data.adn.life/user/profile/{}", pubkey)).ok()?;
	if let Ok(data) = simd_json::serde::from_reader::<_, serde_json::Value>(&mut resp) {
		Some(CesiumplusAccount {
			title: data
				.pointer("/_source/title")
				.and_then(|v| Some(v.as_str()?.to_string())),
			location_pos: data
				.pointer("/_source/geoPoint")
				.and_then(|v| Some((v["lat"].as_f64()?, v["lon"].as_f64()?))),
			location_title: data
				.pointer("/_source/city")
				.and_then(|v| Some(v.as_str()?.to_string())),
			avatar: data
				.pointer("/_source/avatar/_content")
				.and_then(|v| base64::decode(v.as_str()?).ok()),
		})
	} else {
		None
	}
}
