pub mod dup;
pub mod mock;
// pub mod substrate;

use crate::common::*;

pub trait BcReader<C: blockchain::Config>: Send + Sync {
	fn available_blocks(&self) -> u32;
	fn current_block(&self) -> Option<&blockchain::Block<C>>;
	fn get_block(&self, number: C::BlockNumber) -> Option<&blockchain::Block<C>>;
	fn state(&self) -> &state::State<C>;
	fn state_mut(&mut self) -> &mut state::State<C>;
	fn apply_one(&mut self) -> Option<state::Changeset<C>>;
	fn revert_one(&mut self) -> Option<state::Changeset<C>>;
}
