use super::*;

use bevy::prelude::*;
use dashmap::DashMap;
use num_traits::{One, Zero};
use std::{collections::BTreeMap, ops::Bound};

#[derive(Clone, Component, Debug)]
pub struct Cert<C: blockchain::Config> {
	pub created: C::BlockNumber,
	pub issuer: IdtyId,
	pub receiver: IdtyId,
}

#[derive(Clone, Component, Debug)]
pub struct Idty<C: blockchain::Config> {
	pub created: C::BlockNumber,
	pub is_member: bool,
	pub joined: C::BlockNumber,
	pub pubkey: C::Pubkey,
	pub username: String,
}

#[derive(Clone, Debug)]
pub struct IdtyMetadata {
	pub issued_certs_nb: u32,
	pub received_certs_nb: u32,
}

#[derive(Clone, Debug, Default)]
pub struct Changeset<C: blockchain::Config> {
	pub new_certs: DashMap<CertId, Cert<C>>,
	pub new_idties: DashMap<IdtyId, Idty<C>>,
	pub removed_certs: DashMap<CertId, Cert<C>>,
	pub removed_idties: DashMap<IdtyId, Idty<C>>,
}

#[derive(Event)]
pub struct BlockchainEvent<C: blockchain::Config> {
	pub changeset: Changeset<C>,
}

#[derive(Clone)]
pub struct State<C: blockchain::Config> {
	pub certs: DashMap<CertId, Cert<C>>,
	pub certs_issued: BTreeMap<(IdtyId, IdtyId), CertId>,
	pub certs_received: BTreeMap<(IdtyId, IdtyId), CertId>,
	pub idties: DashMap<IdtyId, Idty<C>>,
	pub idties_by_pubkey: DashMap<C::Pubkey, IdtyId>,
	pub idties_metadata: DashMap<IdtyId, IdtyMetadata>,
	pub next_block_number: C::BlockNumber,
	pub next_cert_id: CertId,
	pub next_idty_id: IdtyId,
}

impl<C: blockchain::Config> State<C> {
	pub fn empty() -> Self {
		Self {
			certs: DashMap::new(),
			certs_issued: BTreeMap::new(),
			certs_received: BTreeMap::new(),
			idties: DashMap::new(),
			idties_by_pubkey: DashMap::new(),
			idties_metadata: DashMap::new(),
			next_block_number: C::BlockNumber::zero(),
			next_cert_id: CertId(0),
			next_idty_id: IdtyId(0),
		}
	}

	pub fn apply_block(&mut self, block: &blockchain::Block<C>) -> Changeset<C> {
		assert_eq!(block.number, self.next_block_number);
		let changeset = Changeset::default();
		for bc_idty in block.idties.iter() {
			let idty = Idty {
				created: bc_idty.block_number,
				is_member: true,
				joined: block.number,
				pubkey: bc_idty.pubkey,
				username: bc_idty.username.clone(),
			};
			self.idties.insert(self.next_idty_id, idty.clone());
			self.idties_by_pubkey.insert(idty.pubkey, self.next_idty_id);
			self.idties_metadata.insert(
				self.next_idty_id,
				IdtyMetadata {
					issued_certs_nb: 0,
					received_certs_nb: 0,
				},
			);
			changeset.new_idties.insert(self.next_idty_id, idty);
			self.next_idty_id.0 += 1;
		}
		for bc_cert in block.certs.iter() {
			let cert = Cert {
				created: bc_cert.blockstamp,
				issuer: *self
					.idties_by_pubkey
					.get(&bc_cert.issuer)
					.expect("corrupted state"),
				receiver: *self
					.idties_by_pubkey
					.get(&bc_cert.receiver)
					.expect("corrupted state"),
			};
			self.certs.insert(self.next_cert_id, cert.clone());
			self.certs_issued
				.insert((cert.issuer, cert.receiver), self.next_cert_id);
			self.certs_received
				.insert((cert.receiver, cert.issuer), self.next_cert_id);
			self.idties_metadata
				.get_mut(&cert.issuer)
				.expect("corrupted state")
				.issued_certs_nb += 1;
			self.idties_metadata
				.get_mut(&cert.receiver)
				.expect("corrupted state")
				.received_certs_nb += 1;
			changeset.new_certs.insert(self.next_cert_id, cert);
			self.next_cert_id.0 += 1;
		}
		for pubkey in block.excluded.iter() {
			let idty_id = self.idties_by_pubkey.get(pubkey).expect("corrupted state");
			let mut idty = self.idties.get_mut(&idty_id).expect("corrupted state");
			idty.is_member = false;
		}
		self.next_block_number += C::BlockNumber::one();
		changeset
	}

	pub fn revert_block(&mut self, block: &blockchain::Block<C>) -> Changeset<C> {
		assert_eq!(block.number, self.next_block_number - C::BlockNumber::one());
		let changeset = Changeset::default();
		for pubkey in block.excluded.iter().rev() {
			let idty_id = self.idties_by_pubkey.get(pubkey).expect("corrupted state");
			let mut idty = self.idties.get_mut(&idty_id).expect("corrupted state");
			idty.is_member = true;
		}
		for bc_cert in block.certs.iter().rev() {
			self.next_cert_id.0 -= 1;
			let cert = self
				.certs
				.remove(&self.next_cert_id)
				.expect("corrupted state")
				.1;
			assert_eq!(
				bc_cert.issuer,
				self.idties
					.get(&cert.issuer)
					.expect("corrupted state")
					.pubkey,
				"corrupted state"
			);
			assert_eq!(
				bc_cert.receiver,
				self.idties
					.get(&cert.receiver)
					.expect("corrupted state")
					.pubkey,
				"corrupted state"
			);
			self.certs_issued
				.remove(&(cert.issuer, cert.receiver))
				.expect("corrupted state");
			self.certs_received
				.remove(&(cert.receiver, cert.issuer))
				.expect("corrupted state");
			self.idties_metadata
				.get_mut(&cert.issuer)
				.expect("corrupted state")
				.issued_certs_nb -= 1;
			self.idties_metadata
				.get_mut(&cert.receiver)
				.expect("corrupted state")
				.received_certs_nb -= 1;
			changeset.removed_certs.insert(self.next_cert_id, cert);
		}
		for bc_idty in block.idties.iter().rev() {
			self.next_idty_id.0 -= 1;
			let idty_id = self
				.idties_by_pubkey
				.remove(&bc_idty.pubkey)
				.expect("corrupted state")
				.1;
			assert_eq!(idty_id, self.next_idty_id, "corrupted state");
			let idty = self.idties.remove(&idty_id).expect("corrupted state").1;
			assert_eq!(idty.pubkey, bc_idty.pubkey, "corrupted state");
			self.idties_metadata
				.remove(&idty_id)
				.expect("corrupted state");
			changeset.removed_idties.insert(idty_id, idty);
		}
		self.next_block_number -= C::BlockNumber::one();
		changeset
	}

	pub fn _certs_issued_iter(
		&self,
		idty_id: IdtyId,
	) -> std::collections::btree_map::Range<(IdtyId, IdtyId), CertId> {
		self.certs_issued.range((
			Bound::Included((idty_id, IdtyId(0))),
			Bound::Included((idty_id, IdtyId(u32::MAX))),
		))
	}

	pub fn _certs_issued_iter_mut(
		&mut self,
		idty_id: IdtyId,
	) -> std::collections::btree_map::RangeMut<(IdtyId, IdtyId), CertId> {
		self.certs_issued.range_mut((
			Bound::Included((idty_id, IdtyId(0))),
			Bound::Included((idty_id, IdtyId(u32::MAX))),
		))
	}

	pub fn _certs_received_iter(
		&self,
		idty_id: IdtyId,
	) -> std::collections::btree_map::Range<(IdtyId, IdtyId), CertId> {
		self.certs_received.range((
			Bound::Included((idty_id, IdtyId(0))),
			Bound::Included((idty_id, IdtyId(u32::MAX))),
		))
	}

	pub fn _certs_received_iter_mut(
		&mut self,
		idty_id: IdtyId,
	) -> std::collections::btree_map::RangeMut<(IdtyId, IdtyId), CertId> {
		self.certs_received.range_mut((
			Bound::Included((idty_id, IdtyId(0))),
			Bound::Included((idty_id, IdtyId(u32::MAX))),
		))
	}
}
