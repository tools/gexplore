use bevy::prelude::*;
use dashmap::DashMap;
use num_traits::{AsPrimitive, One, PrimInt};
use std::{
	fmt::{Debug, Display},
	hash::Hash,
	ops::{AddAssign, SubAssign},
};

#[derive(Clone, Component, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct CertId(pub u32);

#[derive(Clone, Component, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct IdtyId(pub u32);

#[derive(Clone, Debug)]
pub struct Idty<C: Config> {
	pub block_number: C::BlockNumber,
	pub pubkey: C::Pubkey,
	pub username: String,
}

#[derive(Clone, Debug)]
pub struct Cert<C: Config> {
	pub blockstamp: C::BlockNumber,
	pub issuer: C::Pubkey,
	pub receiver: C::Pubkey,
}

pub trait Config: Clone + Default {
	type Pubkey: Clone + Copy + Debug + Default + Display + Eq + Hash + PartialEq + Send + Sync;
	type BlockNumber: Clone
		+ Copy
		+ Debug
		+ Display
		+ Eq
		+ Hash
		+ PrimInt
		+ AsPrimitive<usize>
		+ AsPrimitive<u32>
		+ AsPrimitive<f32>
		+ One
		+ AddAssign
		+ SubAssign
		+ Send
		+ Sync;
}

#[derive(Clone, Debug)]
pub struct Block<C: Config> {
	pub certs: Vec<Cert<C>>,
	pub excluded: Vec<C::Pubkey>,
	pub hash: [u8; 32],
	pub idties: Vec<Idty<C>>,
	pub number: C::BlockNumber,
	pub median_time: u64,
}
