mod bc_dup;
mod bc_mock;
mod blockchain;
mod cli;
mod measure;
mod offchain;
mod state;
mod util;

use crate::{blockchain::BcReader, measure::traits::*};

use bevy::{
	input::{keyboard::KeyboardInput, ButtonState},
	prelude::*,
	window::WindowMode,
};
use bevy_atmosphere::prelude::*;
//use bevy_easings::*;
use bevy_egui::{egui, EguiContext, EguiPlugin};
use bevy_mod_picking::{
	events::Pointer,
	prelude::*,
	selection::{Deselect, Select},
};
use bevy_polyline::prelude::*;
use chrono::{TimeZone, Utc};
use clap::Parser;
use dashmap::DashMap;
use enterpolation::{Curve, Generator as _};
use epaint::textures::TextureOptions;
use num_traits::AsPrimitive;
use palette::{convert::IntoColorUnclamped, IntoColor};
use rand::distributions::Distribution;
use rayon::iter::ParallelIterator;
use sha2::Digest;
use smooth_bevy_cameras::{
	controllers::orbit::{OrbitCameraBundle, OrbitCameraController, OrbitCameraPlugin},
	LookTransform, LookTransformPlugin,
};
use std::{fmt::Debug, io::Write, marker::PhantomData};

#[derive(Default, Resource)]
struct Entities {
	certs: DashMap<state::CertId, Entity>,
	idties: DashMap<state::IdtyId, Entity>,
}

#[derive(Resource)]
struct LayoutModule {
	layout: forceatlas2::Layout<f32, 3>,
	idty_map: DashMap<state::IdtyId, usize>,
}

#[derive(Resource)]
struct UiState {
	avatar_handle: Option<egui::TextureHandle>,
	blockchain_speed: u32,
	cert_colors: [epaint::Hsva; 2],
	idty_colors: [epaint::Hsva; 2],
	idty_radiuses: [f32; 2],
	layout_enabled: bool,
	layout_ka: f32,
	layout_kg: f32,
	layout_kr: f32,
	layout_speed: f32,
	measure_cert_color_index: usize,
	measure_idty_color_index: usize,
	measure_idty_radius_index: usize,
	selected_idty: Option<state::IdtyId>,
	target_block_number: u32,
}

impl Default for UiState {
	fn default() -> Self {
		Self {
			avatar_handle: None,
			blockchain_speed: 1,
			cert_colors: [
				epaint::Hsva {
					h: 0.0,
					s: 0.0,
					v: 0.0,
					a: 0.2,
				},
				epaint::Hsva {
					h: 0.0,
					s: 0.0,
					v: 0.0,
					a: 0.8,
				},
			],
			idty_colors: [
				epaint::Hsva {
					h: 0.0,
					s: 1.0,
					v: 1.0,
					a: 1.0,
				},
				epaint::Hsva {
					h: 0.5,
					s: 1.0,
					v: 1.0,
					a: 1.0,
				},
			],
			idty_radiuses: [1.0, 10.0],
			layout_enabled: true,
			layout_ka: 1.0,
			layout_kg: 1.0,
			layout_kr: 1.0,
			layout_speed: 0.01,
			measure_cert_color_index: 0,
			measure_idty_color_index: 0,
			measure_idty_radius_index: 0,
			selected_idty: None,
			target_block_number: 0,
		}
	}
}

#[derive(Resource)]
struct AssetHandles {
	cert_polyline_materials: [Handle<PolylineMaterial>; 100],
	idty_sphere_materials: [Handle<StandardMaterial>; 100],
	idty_sphere_meshes: [Handle<Mesh>; 100],
}

#[derive(Clone, Debug, Eq, Hash, PartialEq, SystemSet)]
enum Set {
	UpdatePositions,
	UpdateState,
}

#[derive(Event)]
enum WotSelectionEvent {
	Select(Entity),
	Deselect(Entity),
}

impl From<ListenerInput<Pointer<Select>>> for WotSelectionEvent {
	fn from(event: ListenerInput<Pointer<Select>>) -> Self {
		Self::Select(event.target)
	}
}

impl From<ListenerInput<Pointer<Deselect>>> for WotSelectionEvent {
	fn from(event: ListenerInput<Pointer<Deselect>>) -> Self {
		Self::Deselect(event.target)
	}
}

fn main() {
	let opt = cli::MainOpt::<bc_dup::BcOpt>::parse();

	//let bc_reader = bc_mock::BcMock::new(1000);
	let bc_reader = bc_dup::BcDup::new(&opt.bc_opt);

	Gexplore::run(bc_reader);
}

struct Gexplore<C: 'static + Debug + blockchain::Config, R: 'static + BcReader<C>> {
	_p: PhantomData<(C, R)>,
}

#[allow(clippy::too_many_arguments)]
impl<C: 'static + Debug + blockchain::Config, R: 'static + BcReader<C> + Resource> Gexplore<C, R> {
	fn run(bc_reader: R) {
		let entities = Entities::default();
		let layout_module = LayoutModule {
			layout: forceatlas2::Layout::<f32, 3>::empty(Default::default()),
			idty_map: DashMap::new(),
		};

		App::new()
			.insert_resource(layout_module)
			.insert_resource(bc_reader)
			.insert_resource(entities)
			.insert_resource(measure::idty::MeasureIdtyList::new())
			.insert_resource(measure::cert::MeasureCertList::new())
			.init_resource::<UiState>()
			.init_resource::<offchain::OffchainData<C>>()
			//.insert_resource(Msaa { samples: 1 })
			.add_plugins((
				DefaultPlugins,
				//.add_plugin(EasingsPlugin)
				LookTransformPlugin,
				OrbitCameraPlugin::default(),
				DefaultPickingPlugins,
				PolylinePlugin,
				EguiPlugin,
				AtmospherePlugin,
			))
			.add_event::<WotSelectionEvent>()
			.add_systems(Startup, Self::setup)
			.configure_sets(Update, Set::UpdatePositions)
			.configure_sets(PreUpdate, Set::UpdateState)
			.add_systems(
				Update,
				(
					Self::update_positions.in_set(Set::UpdatePositions),
					Self::keyboard_event_system,
					Self::ui,
					Self::wot_selection_event_reader.run_if(on_event::<WotSelectionEvent>()),
				),
			)
			.add_systems(PreUpdate, Self::update_state.in_set(Set::UpdateState))
			.run();
	}

	fn setup(
		mut windows: Query<&mut Window>,
		mut commands: Commands,
		mut meshes: ResMut<Assets<Mesh>>,
		mut materials: ResMut<Assets<StandardMaterial>>,
		mut polyline_materials: ResMut<Assets<PolylineMaterial>>,
	) {
		windows.single_mut().title = String::from("ğexplore");

		commands.insert_resource(AmbientLight {
			color: Color::WHITE,
			brightness: 0.6,
		});

		commands.insert_resource(AssetHandles {
			cert_polyline_materials: (0..100)
				.map(|_| {
					polyline_materials.add(PolylineMaterial {
						width: 0.5,
						color: Color::Hsla {
							hue: 0.0,
							saturation: 0.0,
							lightness: 0.0,
							alpha: 0.4,
						},
						perspective: false,
						depth_bias: 0.0,
					})
				})
				.collect::<Vec<_>>()
				.try_into()
				.unwrap(),
			idty_sphere_materials: (0..100)
				.map(|_| {
					materials.add(StandardMaterial {
						perceptual_roughness: 0.5,
						metallic: 0.6,
						base_color: Color::RED,
						..Default::default()
					})
				})
				.collect::<Vec<_>>()
				.try_into()
				.unwrap(),
			idty_sphere_meshes: (0..100)
				.map(|_| {
					meshes.add(
						Mesh::try_from(shape::Icosphere {
							radius: 1.0,
							subdivisions: 1,
						})
						.unwrap(),
					)
				})
				.collect::<Vec<_>>()
				.try_into()
				.unwrap(),
		});

		commands
			.spawn(OrbitCameraBundle::new(
				OrbitCameraController::default(),
				Vec3::new(0.0, -100.0, 0.0),
				Vec3::new(0.0, 0.0, 0.0),
				Vec3::new(0.0, 0.0, 1.0),
			))
			.insert(Camera3dBundle::default())
			.insert(AtmosphereCamera::default());
	}

	fn apply_state_change(
		changeset: state::Changeset<C>,
		commands: &mut Commands,
		entities: &mut ResMut<Entities>,
		layout_module: &mut ResMut<LayoutModule>,
		polylines: &mut ResMut<Assets<Polyline>>,
		asset_handles: &Res<AssetHandles>,
		query_idty: &mut Query<(&state::IdtyId, &Transform)>,
	) {
		let mut rng = rand::thread_rng();
		let node_spawn_distrib = rand::distributions::Uniform::<f32>::new(-1.0, 1.0);

		for (idty_id, _idty) in changeset.new_idties {
			entities.idties.insert(
				idty_id,
				commands
					.spawn(PbrBundle {
						mesh: asset_handles.idty_sphere_meshes[0].clone(),
						material: asset_handles.idty_sphere_materials[0].clone(),
						..Default::default()
					})
					.insert(idty_id)
					.insert(PickableBundle::default())
					.insert(On::<Pointer<Select>>::send_event::<WotSelectionEvent>())
					.insert(On::<Pointer<Deselect>>::send_event::<WotSelectionEvent>())
					.id(),
			);
			let nb_idties = layout_module.layout.nodes.len();

			// Place new idty at its certifiers' barycenter
			let mut pos = [
				node_spawn_distrib.sample(&mut rng),
				node_spawn_distrib.sample(&mut rng),
				node_spawn_distrib.sample(&mut rng),
			];
			let mut nb_certifiers: usize = 0;
			for cert in changeset.new_certs.iter() {
				let cert = cert.value();
				if cert.receiver == idty_id {
					if let Some(issuer) = entities.idties.get(&cert.issuer) {
						if let Ok(issuer) = query_idty.get(*issuer) {
							nb_certifiers += 1;
							pos[0] += issuer.1.translation[0];
							pos[1] += issuer.1.translation[1];
							pos[2] += issuer.1.translation[2];
						}
					}
				}
			}
			if nb_certifiers > 0 {
				let nb_certifiers = nb_certifiers as f32;
				pos[0] /= nb_certifiers;
				pos[1] /= nb_certifiers;
				pos[2] /= nb_certifiers;
			}

			layout_module.layout.add_nodes(
				&[],
				&[forceatlas2::Node {
					pos: forceatlas2::VecN(pos),
					..Default::default()
				}],
			);
			layout_module.idty_map.insert(idty_id, nb_idties);
		}
		for (cert_id, cert) in changeset.new_certs {
			entities.certs.insert(
				cert_id,
				commands
					.spawn(PolylineBundle {
						polyline: polylines.add(Polyline {
							vertices: vec![Vec3::ZERO, Vec3::ZERO],
						}),
						material: asset_handles.cert_polyline_materials[0].clone(),
						..Default::default()
					})
					.insert(cert_id)
					//.insert(PickableBundle::default())
					.id(),
			);
			let edge = (
				*layout_module
					.idty_map
					.get(&cert.issuer)
					.expect("corrupted state"),
				*layout_module
					.idty_map
					.get(&cert.receiver)
					.expect("corrupted state"),
			);
			layout_module.layout.add_nodes(&[(edge, 1.0)], &[]);
		}
		'l1: for (cert_id, cert) in changeset.removed_certs {
			commands
				.entity(entities.certs.remove(&cert_id).expect("corrupted state").1)
				.remove::<PolylineBundle>();
			let edge = (
				*layout_module
					.idty_map
					.get(&cert.issuer)
					.expect("corrupted state"),
				*layout_module
					.idty_map
					.get(&cert.receiver)
					.expect("corrupted state"),
			);
			layout_module.layout.nodes[edge.0].mass -= 1.0;
			layout_module.layout.nodes[edge.1].mass -= 1.0;
			for i in 0..layout_module.layout.edges.len() {
				if layout_module.layout.edges[i].0 == edge {
					layout_module.layout.edges.remove(i);
					continue 'l1;
				}
			}
			panic!("corrupted state");
		}
		for (idty_id, _idty) in changeset.removed_idties {
			commands
				.entity(entities.idties.remove(&idty_id).expect("corrupted state").1)
				.remove::<PbrBundle>();
			let node_id = layout_module
				.idty_map
				.remove(&idty_id)
				.expect("corrupted state")
				.1;
			layout_module
				.idty_map
				.par_iter_mut()
				.filter(|o| **o > node_id)
				.for_each(|mut o| *o -= 1);
			layout_module.layout.remove_node(node_id);
		}
	}

	fn update_state(
		mut commands: Commands,
		mut bc_reader: ResMut<R>,
		mut entities: ResMut<Entities>,
		mut layout_module: ResMut<LayoutModule>,
		mut polylines: ResMut<Assets<Polyline>>,
		ui_state: Res<UiState>,
		asset_handles: Res<AssetHandles>,
		mut materials: ResMut<Assets<StandardMaterial>>,
		mut polyline_materials: ResMut<Assets<PolylineMaterial>>,
		mut meshes: ResMut<Assets<Mesh>>,
		mut query_idty_material: Query<(&state::IdtyId, &mut Handle<StandardMaterial>)>,
		mut query_idty_mesh: Query<(&state::IdtyId, &mut Handle<Mesh>)>,
		mut query_idty_transform: Query<(&state::IdtyId, &Transform)>,
		// mut query_cert: Query<(&state::CertId, &mut Handle<PolylineMaterial>)>,
		mut measure_idty_list: ResMut<measure::idty::MeasureIdtyList>,
		mut measure_cert_list: ResMut<measure::cert::MeasureCertList>,
	) {
		// Measure idty color
		/*let gradient = enterpolation::linear::Linear::builder()
		.elements(
			ui_state.idty_colors[0..measure_idty_list.list[ui_state.measure_idty_color_index]
				.0
				.points
				.len()]
				.iter()
				.map(|color_hsv| palette::Luva {
					color: palette::Hsv::<palette::encoding::Srgb>::from_components((
						color_hsv.h * 360.,
						color_hsv.s,
						color_hsv.v,
					))
					.into_color_unclamped(),
					alpha: color_hsv.a,
				})
				.collect::<Vec<_>>(),
		)
		.equidistant()
		.normalized()
		.build()
		.unwrap();*/
		for (_i, handle) in asset_handles.idty_sphere_materials.iter().enumerate() {
			// let color_hsl: palette::Alpha<palette::Hsl, f32> =
			// 	gradient.gen(i as f32 / 100.).into_color();
			let color_hsl: palette::Alpha<palette::Hsl, f32> = palette::Alpha {
				color: palette::Hsl::from_components((0.0, 1.0, 1.0)),
				alpha: 1.0,
			};
			materials.get_mut(handle).unwrap().base_color = Color::Hsla {
				hue: color_hsl.hue.into_positive_degrees(),
				saturation: color_hsl.saturation,
				lightness: color_hsl.lightness,
				alpha: color_hsl.alpha,
			};
		}
		let measure = &mut measure_idty_list.list[ui_state.measure_idty_color_index].1;
		measure.prepare(&bc_reader);
		for (idty_id, mut idty_material) in query_idty_material.iter_mut() {
			let idty = bc_reader
				.state()
				.idties
				.get(idty_id)
				.expect("corrupted state");
			let value = measure.compute_one(&bc_reader, *idty.key());
			*idty_material =
				asset_handles.idty_sphere_materials[((value * 100.) as usize).min(99)].clone();
		}

		// Measure idty radius
		if measure_idty_list.list[ui_state.measure_idty_radius_index]
			.0
			.points
			.len() == 1
		{
			for handle in asset_handles.idty_sphere_meshes.iter() {
				*meshes.get_mut(handle).unwrap() = Mesh::try_from(shape::Icosphere {
					radius: ui_state.idty_radiuses[0],
					subdivisions: 1,
				})
				.unwrap();
			}
		} else {
			let gradient = enterpolation::linear::Linear::builder()
				.elements(
					ui_state.idty_radiuses[0..measure_idty_list.list
						[ui_state.measure_idty_radius_index]
						.0
						.points
						.len()]
						.to_vec(),
				)
				.equidistant::<f32>()
				.domain(0.0, 100.0)
				.build()
				.unwrap();
			for (handle, radius) in asset_handles
				.idty_sphere_meshes
				.iter()
				.zip(gradient.take(100))
			{
				*meshes.get_mut(handle).unwrap() = Mesh::try_from(shape::Icosphere {
					radius,
					subdivisions: 1,
				})
				.unwrap();
			}
		}
		let measure = &mut measure_idty_list.list[ui_state.measure_idty_radius_index].1;
		measure.prepare(&bc_reader);
		for (idty_id, mut idty_mesh) in query_idty_mesh.iter_mut() {
			let idty = bc_reader
				.state()
				.idties
				.get(idty_id)
				.expect("corrupted state");
			let value = measure.compute_one(&bc_reader, *idty.key());
			*idty_mesh =
				asset_handles.idty_sphere_meshes[((value * 100.) as usize).min(99)].clone();
		}

		// Measure cert color
		/*let gradient = enterpolation::linear::Linear::builder()
			.elements(
				ui_state.cert_colors[0..measure_cert_list.list[ui_state.measure_cert_color_index]
					.0
					.points
					.len()]
					.iter()
					.map(|color_hsv| palette::Luva {
						color: palette::Hsv::<palette::encoding::Srgb>::from_components((
							color_hsv.h * 360.,
							color_hsv.s,
							color_hsv.v,
						))
						.into_color(),
						alpha: color_hsv.a,
					})
					.collect::<Vec<_>>(),
			)
			.equidistant()
			.normalized()
			.build()
			.unwrap()*/;
		for (_i, handle) in asset_handles.cert_polyline_materials.iter().enumerate() {
			// let color_hsl: palette::Alpha<palette::Hsl, f32> =
			// 	gradient.gen(i as f32 / 100.).into_color();
			let color_hsl: palette::Alpha<palette::Hsl, f32> = palette::Alpha {
				color: palette::Hsl::from_components((0.0, 1.0, 1.0)),
				alpha: 1.0,
			};
			polyline_materials.get_mut(handle).unwrap().color = Color::Hsla {
				hue: color_hsl.hue.into_positive_degrees(),
				saturation: color_hsl.saturation,
				lightness: color_hsl.lightness,
				alpha: color_hsl.alpha,
			};
		}
		let measure = &mut measure_cert_list.list[ui_state.measure_cert_color_index].1;
		measure.prepare(&bc_reader);
		/*for (cert_id, cert_material) in query_cert.iter_mut() {
			let cert = bc_reader
				.state()
				.certs
				.get(cert_id)
				.expect("corrupted state");
			let value = measure.compute_one(&bc_reader, *cert.key());
			*cert_material = *polyline_materials
				.get(
					asset_handles.cert_polyline_materials[((value * 100.) as usize).min(99)]
						.clone(),
				)
				.unwrap();
		}*/

		for _ in 0..ui_state.blockchain_speed {
			match (ui_state.target_block_number as u32 + 1)
				.cmp(&bc_reader.state().next_block_number.as_())
			{
				std::cmp::Ordering::Greater => {
					if let Some(changeset) = bc_reader.apply_one() {
						Self::apply_state_change(
							changeset,
							&mut commands,
							&mut entities,
							&mut layout_module,
							&mut polylines,
							&asset_handles,
							&mut query_idty_transform,
						);
					}
				}
				std::cmp::Ordering::Less => {
					if let Some(changeset) = bc_reader.revert_one() {
						Self::apply_state_change(
							changeset,
							&mut commands,
							&mut entities,
							&mut layout_module,
							&mut polylines,
							&asset_handles,
							&mut query_idty_transform,
						);
					}
				}
				std::cmp::Ordering::Equal => return,
			}
		}
	}

	fn keyboard_event_system(
		mut keyboard_input_events: EventReader<KeyboardInput>,
		mut windows: Query<&mut Window>,
		mut ui_state: ResMut<UiState>,
		bc_reader: Res<R>,
	) {
		for event in keyboard_input_events.read() {
			match event {
				KeyboardInput {
					key_code: KeyCode::F11,
					state: ButtonState::Released,
					..
				} => Self::toggle_fullscreen(&mut windows),
				KeyboardInput {
					key_code: KeyCode::NumpadAdd,
					state: ButtonState::Pressed,
					..
				} => {
					if ui_state.target_block_number < bc_reader.available_blocks() - 1 {
						ui_state.target_block_number += 1;
					}
				}
				KeyboardInput {
					key_code: KeyCode::NumpadSubtract,
					state: ButtonState::Pressed,
					..
				} => {
					if ui_state.target_block_number > 0 {
						ui_state.target_block_number -= 1;
					}
				}
				_ => {}
			}
		}
	}

	fn wot_selection_event_reader(
		mut events: EventReader<WotSelectionEvent>,
		mut cameras: Query<&mut LookTransform>,
		query_idty: Query<(&state::IdtyId, &Transform)>,
		mut ui_state: ResMut<UiState>,
	) {
		for event in events.read() {
			match event {
				WotSelectionEvent::Select(target) => {
					if let Ok((idty_id, idty_pos)) = query_idty.get(*target) {
						for mut camera in cameras.iter_mut() {
							camera.target = idty_pos.translation;
							ui_state.selected_idty = Some(*idty_id);
							ui_state.avatar_handle = None;
						}
					}
				}
				WotSelectionEvent::Deselect(target) => {
					if let Ok((idty_id, _idty_pos)) = query_idty.get(*target) {
						if Some(*idty_id) == ui_state.selected_idty {
							ui_state.selected_idty = None;
							ui_state.avatar_handle = None;
						}
					}
				}
			}
		}
	}

	fn toggle_fullscreen(windows: &mut Query<&mut Window>) {
		let mut window = windows.single_mut();
		window.mode = match window.mode {
			WindowMode::Windowed => WindowMode::Fullscreen,
			_ => WindowMode::Windowed,
		};
	}

	fn update_positions(
		bc_reader: Res<R>,
		entities: Res<Entities>,
		mut layout_module: ResMut<LayoutModule>,
		mut polylines: ResMut<Assets<Polyline>>,
		mut query_idty: Query<(&state::IdtyId, &mut Transform)>,
		query_cert: Query<&Handle<Polyline>>,
		ui_state: Res<UiState>,
	) {
		// TODO do this only on event
		let mut layout_settings = layout_module.layout.get_settings().clone();
		layout_settings.speed = ui_state.layout_speed;
		layout_settings.ka = ui_state.layout_ka;
		layout_settings.kg = ui_state.layout_kg;
		layout_settings.kr = ui_state.layout_kr;
		layout_module.layout.set_settings(layout_settings);

		if ui_state.layout_enabled {
			layout_module.layout.iteration();
			for (idty_id, mut idty_pos) in query_idty.iter_mut() {
				let point =
					layout_module.layout.nodes[*layout_module.idty_map.get(idty_id).unwrap()].pos;
				idty_pos.translation[0] = point[0];
				idty_pos.translation[1] = point[1];
				idty_pos.translation[2] = point[2];
			}

			bc_reader.state().certs.iter().for_each(|o| {
				let (cert_id, cert) = o.pair();
				let cert_transform = polylines
					.get_mut(
						query_cert
							.get(*entities.certs.get(cert_id).unwrap())
							.unwrap(),
					)
					.unwrap();
				cert_transform.vertices[0] = query_idty
					.get(*entities.idties.get(&cert.issuer).unwrap())
					.unwrap()
					.1
					.translation;
				cert_transform.vertices[1] = query_idty
					.get(*entities.idties.get(&cert.receiver).unwrap())
					.unwrap()
					.1
					.translation;
			});
		}
	}

	fn ui(
		mut egui_context: Query<&mut EguiContext>,
		mut ui_state: ResMut<UiState>,
		bc_reader: Res<R>,
		mut offchain_data: ResMut<offchain::OffchainData<C>>,
		measure_idty_list: Res<measure::idty::MeasureIdtyList>,
		measure_cert_list: Res<measure::cert::MeasureCertList>,
	) {
		let mut egui_context = egui_context.single_mut();
		let egui_context = egui_context.get_mut();
		egui::Window::new("Blockchain").show(egui_context, |ui| {
			ui.add(
				egui::Slider::new(&mut ui_state.blockchain_speed, 1..=1000)
					.logarithmic(true)
					.text("speed"),
			);
			if let Some(current_block) = bc_reader.current_block() {
				ui.label(format!(
					"Block: {} / {}\n{}",
					current_block.number,
					bc_reader.available_blocks(),
					Utc.timestamp_opt(current_block.median_time as i64, 0)
						.unwrap()
						.to_rfc3339()
				));
			} else {
				ui.label(format!("Block: - / {}", bc_reader.available_blocks()));
			}
			ui.add(egui::Slider::new(
				&mut ui_state.target_block_number,
				0..=bc_reader.available_blocks() - 1,
			));
			ui.label(format!("Identities: {}", bc_reader.state().idties.len()));
			ui.label(format!("Certifications: {}", bc_reader.state().certs.len()));
		});
		egui::Window::new("Layout").show(egui_context, |ui| {
			ui.checkbox(&mut ui_state.layout_enabled, "Enabled");
			ui.add(
				egui::Slider::new(&mut ui_state.layout_speed, 0.00001..=10.)
					.logarithmic(true)
					.text("speed"),
			);
			ui.add(
				egui::Slider::new(&mut ui_state.layout_ka, 0.001..=100.)
					.logarithmic(true)
					.text("attraction"),
			);
			ui.add(
				egui::Slider::new(&mut ui_state.layout_kr, 0.001..=100.)
					.logarithmic(true)
					.text("repulsion"),
			);
			ui.add(
				egui::Slider::new(&mut ui_state.layout_kg, 0.001..=100.)
					.logarithmic(true)
					.text("gravity"),
			);
		});
		egui::Window::new("Appearance").show(egui_context, |ui| {
			egui::containers::ComboBox::from_label("Identity color")
				.selected_text(
					measure_idty_list.list[ui_state.measure_idty_color_index]
						.0
						.title,
				)
				.show_ui(ui, |ui| {
					for (i, desc) in measure_idty_list.list.iter().enumerate() {
						ui.selectable_value(
							&mut ui_state.measure_idty_color_index,
							i,
							desc.0.title,
						);
					}
				});
			ui.horizontal(|ui| {
				for (desc, color) in measure_idty_list.list[ui_state.measure_idty_color_index]
					.0
					.points
					.iter()
					.zip(ui_state.idty_colors.iter_mut())
				{
					egui::widgets::color_picker::color_edit_button_hsva(
						ui,
						color,
						egui::widgets::color_picker::Alpha::OnlyBlend,
					);
					ui.label(*desc);
				}
			});
			ui.separator();
			egui::containers::ComboBox::from_label("Identity radius")
				.selected_text(
					measure_idty_list.list[ui_state.measure_idty_radius_index]
						.0
						.title,
				)
				.show_ui(ui, |ui| {
					for (i, desc) in measure_idty_list.list.iter().enumerate() {
						ui.selectable_value(
							&mut ui_state.measure_idty_radius_index,
							i,
							desc.0.title,
						);
					}
				});
			ui.horizontal(|ui| {
				for (desc, radius) in measure_idty_list.list[ui_state.measure_idty_radius_index]
					.0
					.points
					.iter()
					.zip(ui_state.idty_radiuses.iter_mut())
				{
					ui.add(egui::Slider::new(radius, 1.0..=100.0).text(*desc));
				}
			});
			ui.separator();
			egui::containers::ComboBox::from_label("Certification color")
				.selected_text(
					measure_cert_list.list[ui_state.measure_cert_color_index]
						.0
						.title,
				)
				.show_ui(ui, |ui| {
					for (i, desc) in measure_cert_list.list.iter().enumerate() {
						ui.selectable_value(
							&mut ui_state.measure_cert_color_index,
							i,
							desc.0.title,
						);
					}
				});
			ui.horizontal(|ui| {
				for (desc, color) in measure_cert_list.list[ui_state.measure_cert_color_index]
					.0
					.points
					.iter()
					.zip(ui_state.cert_colors.iter_mut())
				{
					egui::widgets::color_picker::color_edit_button_hsva(
						ui,
						color,
						egui::widgets::color_picker::Alpha::OnlyBlend,
					);
					ui.label(*desc);
				}
			});
		});
		if let Some(idty_id) = &ui_state.selected_idty {
			if let Some(idty) = bc_reader.state().idties.get(idty_id) {
				egui::Window::new("Identity").show(egui_context, |ui| {
					let cesiumplus_account = offchain_data.get_cesiumplus_account(idty.pubkey);
					ui.label(format!("{}", idty.pubkey));
					ui.label(&idty.username);
					ui.label(if idty.is_member {
						"Member"
					} else {
						"Not member"
					});
					ui.separator();
					if let Some(title) = &cesiumplus_account.title {
						ui.label(title);
					}
					if let Some(location_title) = &cesiumplus_account.location_title {
						ui.label(location_title);
					}
					if let Some(location_pos) = &cesiumplus_account.location_pos {
						ui.label(format!("{}, {}", location_pos.0, location_pos.1));
					}
					if let Some(avatar) = &cesiumplus_account.avatar {
						if ui_state.avatar_handle.is_none() {
							if let Ok(img) = image::load_from_memory(avatar) {
								let img = egui::ColorImage::from_rgba_unmultiplied(
									[img.width() as _, img.height() as _],
									img.to_rgba8().as_flat_samples().as_slice(),
								);
								ui_state.avatar_handle = Some(ui.ctx().load_texture(
									"avatar",
									img,
									TextureOptions::default(),
								));
							}
						}
						if let Some(avatar_handle) = &ui_state.avatar_handle {
							ui.image(avatar_handle);
						}
					}
					ui.hyperlink_to(
						"Cesium",
						format!("https://demo.cesium.app/#/app/wot/{}/", idty.pubkey),
					);
					ui.hyperlink_to(
						"Ğchange",
						format!("https://www.gchange.fr/#/app/user/{}/", idty.pubkey),
					);
					let mut hasher = sha2::Sha256::new();
					hasher.write_all(idty.username.as_bytes()).unwrap();
					hasher
						.write_all(idty.created.to_string().as_bytes())
						.unwrap();
					hasher.write_all('-'.to_string().as_bytes()).unwrap();
					hasher
						.write_all(
							hex::encode_upper(
								bc_reader
									.get_block(idty.created)
									.expect("corrupted state")
									.hash,
							)
							.as_bytes(),
						)
						.unwrap();
					ui.hyperlink_to(
						"WotWizard",
						format!(
							"https://wotwizard.axiom-team.fr/fr/membre?hash={}",
							hex::encode_upper(hasher.finalize())
						),
					);
				});
			}
		}
	}
}
