use crate::{
	bc_reader::{self, BcReader},
	state::CertId,
};

use bevy::prelude::*;
use num_traits::AsPrimitive;

pub trait MeasureCert<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> {
	/// Called before `compute_one`
	fn prepare(&mut self, bc_reader: &ResMut<R>);
	/// Result in `0.0..1.0`
	fn compute_one(&self, bc_reader: &ResMut<R>, cert_id: CertId) -> f32;
}

pub enum MeasureCertEnum {
	Uniform(CertUniform),
	Age(CertAge),
}

impl Default for MeasureCertEnum {
	fn default() -> Self {
		Self::Uniform(Default::default())
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureCert<C, R>
	for MeasureCertEnum
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		match self {
			Self::Uniform(v) => v.prepare(bc_reader),
			Self::Age(v) => v.prepare(bc_reader),
		}
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, cert_id: CertId) -> f32 {
		match self {
			Self::Uniform(v) => v.compute_one(bc_reader, cert_id),
			Self::Age(v) => v.compute_one(bc_reader, cert_id),
		}
	}
}

#[derive(Resource)]
pub struct MeasureCertList {
	pub list: Vec<(MeasureCertDescription, MeasureCertEnum)>,
}

impl MeasureCertList {
	pub fn new() -> Self {
		Self {
			list: vec![
				(
					MeasureCertDescription {
						title: "Uniform",
						points: &["Value"],
					},
					MeasureCertEnum::Uniform(CertUniform::default()),
				),
				(
					MeasureCertDescription {
						title: "Age",
						points: &["Min", "Max"],
					},
					MeasureCertEnum::Age(CertAge::default()),
				),
			],
		}
	}
}

pub struct MeasureCertDescription {
	pub title: &'static str,
	pub points: &'static [&'static str],
}

pub struct CertUniform {
	pub value: f32,
}

impl Default for CertUniform {
	fn default() -> Self {
		Self { value: 0.0 }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureCert<C, R>
	for CertUniform
{
	fn prepare(&mut self, _bc_reader: &ResMut<R>) {}
	fn compute_one(&self, _bc_reader: &ResMut<R>, _cert_id: CertId) -> f32 {
		self.value
	}
}

pub struct CertAge {
	pub max: f32,
}

impl Default for CertAge {
	fn default() -> Self {
		Self { max: 1.0 }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureCert<C, R>
	for CertAge
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		self.max = bc_reader
			.current_block()
			.map_or(1.0, |block| (block.number + num_traits::One::one()).as_());
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, cert_id: CertId) -> f32 {
		AsPrimitive::<f32>::as_(
			bc_reader
				.state()
				.certs
				.get(&cert_id)
				.expect("corrupted state")
				.created,
		) / self.max
	}
}
