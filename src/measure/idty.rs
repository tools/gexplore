use crate::{
	bc_reader::{self, BcReader},
	state::IdtyId,
};

use bevy::prelude::*;
use num_traits::AsPrimitive;

pub trait MeasureIdty<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> {
	/// Called before `compute_one`
	fn prepare(&mut self, bc_reader: &ResMut<R>);
	/// Result in `0.0..1.0`
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32;
}

pub enum MeasureIdtyEnum {
	Uniform(IdtyUniform),
	OutDegree(IdtyOutDegree),
	InDegree(IdtyInDegree),
	Degree(IdtyDegree),
	Age(IdtyAge),
}

impl Default for MeasureIdtyEnum {
	fn default() -> Self {
		Self::Uniform(Default::default())
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for MeasureIdtyEnum
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		match self {
			Self::Uniform(v) => v.prepare(bc_reader),
			Self::OutDegree(v) => v.prepare(bc_reader),
			Self::InDegree(v) => v.prepare(bc_reader),
			Self::Degree(v) => v.prepare(bc_reader),
			Self::Age(v) => v.prepare(bc_reader),
		}
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32 {
		match self {
			Self::Uniform(v) => v.compute_one(bc_reader, idty_id),
			Self::OutDegree(v) => v.compute_one(bc_reader, idty_id),
			Self::InDegree(v) => v.compute_one(bc_reader, idty_id),
			Self::Degree(v) => v.compute_one(bc_reader, idty_id),
			Self::Age(v) => v.compute_one(bc_reader, idty_id),
		}
	}
}

#[derive(Resource)]
pub struct MeasureIdtyList {
	pub list: Vec<(MeasureIdtyDescription, MeasureIdtyEnum)>,
}

impl MeasureIdtyList {
	pub fn new() -> Self {
		Self {
			list: vec![
				(
					MeasureIdtyDescription {
						title: "Uniform",
						points: &["Value"],
					},
					MeasureIdtyEnum::Uniform(IdtyUniform::default()),
				),
				(
					MeasureIdtyDescription {
						title: "Out Degree",
						points: &["Min", "Max"],
					},
					MeasureIdtyEnum::OutDegree(IdtyOutDegree::default()),
				),
				(
					MeasureIdtyDescription {
						title: "In Degree",
						points: &["Min", "Max"],
					},
					MeasureIdtyEnum::InDegree(IdtyInDegree::default()),
				),
				(
					MeasureIdtyDescription {
						title: "Degree",
						points: &["Min", "Max"],
					},
					MeasureIdtyEnum::Degree(IdtyDegree::default()),
				),
				(
					MeasureIdtyDescription {
						title: "Age",
						points: &["Min", "Max"],
					},
					MeasureIdtyEnum::Age(IdtyAge::default()),
				),
			],
		}
	}
}

pub struct MeasureIdtyDescription {
	pub title: &'static str,
	pub points: &'static [&'static str],
}

pub struct IdtyUniform {
	pub value: f32,
}

impl Default for IdtyUniform {
	fn default() -> Self {
		Self { value: 0.0 }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for IdtyUniform
{
	fn prepare(&mut self, _bc_reader: &ResMut<R>) {}
	fn compute_one(&self, _bc_reader: &ResMut<R>, _idty_id: IdtyId) -> f32 {
		self.value
	}
}

pub struct IdtyOutDegree {
	pub max: f32,
}

impl Default for IdtyOutDegree {
	fn default() -> Self {
		Self { max: 100. }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for IdtyOutDegree
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		let mut max = 1;
		for idty_metadata in bc_reader.state().idties_metadata.iter() {
			let out_degree = idty_metadata.value().issued_certs_nb;
			if out_degree > max {
				max = out_degree;
			}
		}
		self.max = max as f32;
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32 {
		bc_reader
			.state()
			.idties_metadata
			.get(&idty_id)
			.expect("corrupted state")
			.issued_certs_nb as f32
			/ self.max as f32
	}
}

pub struct IdtyInDegree {
	pub max: f32,
}

impl Default for IdtyInDegree {
	fn default() -> Self {
		Self { max: 100. }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for IdtyInDegree
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		let mut max = 1;
		for idty_metadata in bc_reader.state().idties_metadata.iter() {
			let in_degree = idty_metadata.value().received_certs_nb;
			if in_degree > max {
				max = in_degree;
			}
		}
		self.max = max as f32;
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32 {
		bc_reader
			.state()
			.idties_metadata
			.get(&idty_id)
			.expect("corrupted state")
			.received_certs_nb as f32
			/ self.max as f32
	}
}

pub struct IdtyDegree {
	pub max: f32,
}

impl Default for IdtyDegree {
	fn default() -> Self {
		Self { max: 200. }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for IdtyDegree
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		let mut max = 1;
		for idty_metadata in bc_reader.state().idties_metadata.iter() {
			let idties_metadata = idty_metadata.value();
			let degree = idties_metadata.received_certs_nb + idties_metadata.issued_certs_nb;
			if degree > max {
				max = degree;
			}
		}
		self.max = max as f32;
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32 {
		let idty_metadata = bc_reader
			.state()
			.idties_metadata
			.get(&idty_id)
			.expect("corrupted state");
		(idty_metadata.received_certs_nb as f32 + idty_metadata.issued_certs_nb as f32)
			/ self.max as f32
	}
}

pub struct IdtyAge {
	pub max: f32,
}

impl Default for IdtyAge {
	fn default() -> Self {
		Self { max: 1.0 }
	}
}

impl<C: 'static + bc_reader::Config, R: 'static + BcReader<C> + Resource> MeasureIdty<C, R>
	for IdtyAge
{
	fn prepare(&mut self, bc_reader: &ResMut<R>) {
		self.max = bc_reader
			.current_block()
			.map_or(1.0, |block| (block.number + num_traits::One::one()).as_());
	}
	fn compute_one(&self, bc_reader: &ResMut<R>, idty_id: IdtyId) -> f32 {
		AsPrimitive::<f32>::as_(
			bc_reader
				.state()
				.idties
				.get(&idty_id)
				.expect("corrupted state")
				.joined,
		) / self.max
	}
}
