use std::{
	ffi::OsStr,
	path::{Path, PathBuf},
};

#[derive(Clone, Debug)]
pub struct UsablePath(pub PathBuf);

impl From<&OsStr> for UsablePath {
	fn from(v: &OsStr) -> Self {
		Self(PathBuf::from(v))
	}
}

impl ToString for UsablePath {
	fn to_string(&self) -> String {
		String::from(self.0.as_path().to_str().unwrap())
	}
}

impl AsRef<Path> for UsablePath {
	fn as_ref(&self) -> &Path {
		self.0.as_ref()
	}
}
