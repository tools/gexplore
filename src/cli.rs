use clap::Parser;

#[derive(Clone, Debug, Parser)]
#[clap(name = "gexplore")]
pub struct MainOpt<BcOpt: clap::Args> {
	#[clap(flatten)]
	pub bc_opt: BcOpt,
}
