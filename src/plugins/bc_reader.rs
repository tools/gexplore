use crate::{
	bc_reader::{self, BcReader as _},
	common::*,
};

use bevy::prelude::*;
use std::marker::PhantomData;

#[derive(Resource)]
pub struct BcReaderRes<C: blockchain::Config> {
	pub reader: Box<dyn bc_reader::BcReader<C>>,
	pub _p: PhantomData<C>,
}

#[derive(Event)]
pub enum BcReaderCmdEvent {
	ApplyOne,
	RevertOne,
}

#[derive(Default)]
pub struct BcReaderPlugin<C: blockchain::Config> {
	//reader: Box<dyn bc_reader::BcReader<C>>,
	_p: PhantomData<C>,
}

// impl<C: 'static + blockchain::Config + Send + Sync> BcReaderPlugin<C> {
// 	pub fn new(reader: Box<dyn bc_reader::BcReader<C>>) -> Self {
// 		Self {
// 			reader,
// 			_p: Default::default(),
// 		}
// 	}
// }

impl<C: 'static + blockchain::Config + Send + Sync> Plugin for BcReaderPlugin<C> {
	fn build(&self, app: &mut App) {
		app.add_event::<BcReaderCmdEvent>()
			.add_systems(Update, cmd_event_listener::<C>);
	}
}

fn cmd_event_listener<C: 'static + blockchain::Config + Send + Sync>(
	mut cmd_event_reader: EventReader<BcReaderCmdEvent>,
	mut blockchain_event_writer: EventWriter<state::BlockchainEvent<C>>,
	mut bc_reader: ResMut<BcReaderRes<C>>,
) {
	for event in cmd_event_reader.read() {
		match event {
			BcReaderCmdEvent::ApplyOne => {
				if let Some(changeset) = bc_reader.as_mut().reader.apply_one() {
					blockchain_event_writer.send(state::BlockchainEvent {
						changeset
					});
				}
			}
			BcReaderCmdEvent::RevertOne => {
				if let Some(changeset) = bc_reader.as_mut().reader.revert_one() {
					blockchain_event_writer.send(state::BlockchainEvent {
						changeset
					});
				}
			}
		}
	}
}
