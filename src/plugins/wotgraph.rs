use crate::common::*;

use bevy::prelude::*;
use num_traits::Zero;
use std::marker::PhantomData;

#[derive(Default, Resource)]
pub struct LayoutRes(pub forceatlas2::Layout<
f32,
3,
forceatlas2::EdgeBTreeMap<f32, IdtyId>,
forceatlas2::NodeHashMap<f32, 3, IdtyId>,
IdtyId,
>);

#[derive(Bundle)]
struct IdtyNodeBundle {
	idty_id: IdtyId,
	pbr: PbrBundle,
}

#[derive(Default)]
pub struct WotGraphPlugin<C> {
	_p: PhantomData<C>,
}

impl<C: 'static + blockchain::Config + Send + Sync> Plugin
	for WotGraphPlugin<C>
{
	fn build(&self, app: &mut App) {
		app.insert_resource(LayoutRes::default())
		.add_systems(Update, (Self::blockchain_event_listener, Self::iterate_layout, Self::update_positions));
	}
}

impl<C: 'static + blockchain::Config> WotGraphPlugin<C> {
	fn blockchain_event_listener(
		mut blockchain_event_reader: EventReader<state::BlockchainEvent<C>>,
		mut layout: ResMut<LayoutRes>,
		mut commands: Commands,
		mut meshes: ResMut<Assets<Mesh>>,
		mut materials: ResMut<Assets<StandardMaterial>>,
	) {
		for event in blockchain_event_reader.read() {
			let mut rng = rand::thread_rng();
			for cert in event.changeset.removed_certs.iter() {
				let cert = cert.value();
				layout.0.edges.remove(&(cert.issuer, cert.receiver));
			}
			for idty in event.changeset.removed_idties.iter() {
				layout.0.nodes.remove(idty.key());
			}
			for idty in event.changeset.new_idties.iter() {
				let pos = forceatlas2::sample_unit_cube(&mut rng);
				layout.0.nodes.insert(
					*idty.key(),
					forceatlas2::Node {
						pos,
						speed: Zero::zero(),
						old_speed: Zero::zero(),
						size: 1.0,
						mass: 1.0,
					},
				);
				let mesh = meshes.add(Sphere::new(1.0).mesh().ico(3).unwrap());
				commands.spawn(IdtyNodeBundle {
					pbr: PbrBundle {
						transform: Transform {
							translation: pos.0.into(),
							scale: Vec3::splat(1.0),
							..default()
						},
						mesh: mesh.clone(),
						material: materials.add(Color::rgb(1.0, 1.0, 1.0)),
						..default()
					},
					idty_id: *idty.key(),
				});
			}
			for cert in event.changeset.new_certs.iter() {
				let cert = cert.value();
				layout.0.edges.insert((cert.issuer, cert.receiver), 1.0);
			}
		}
	}

	fn iterate_layout(
		mut layout: ResMut<LayoutRes>,) {
		layout.0.iteration();
	}

	fn update_positions(
		mut layout: ResMut<LayoutRes>,
		mut query_idty: Query<(&IdtyId, &mut Transform)>,
	) {
			for (idty_id, mut idty_pos) in query_idty.iter_mut() {
				idty_pos.translation = layout.0.nodes[idty_id].pos.0.into();
			}
	}
}
