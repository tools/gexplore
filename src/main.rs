mod bc_reader;
mod cli;
mod common;
// mod measure;
mod offchain;
mod plugins;
mod util;

use common::*;

use bevy::{
	input::{keyboard::KeyboardInput, ButtonState},
	prelude::*,
	window::WindowMode,
};
use bevy_atmosphere::prelude::*;
use clap::Parser;
use smooth_bevy_cameras::{controllers::orbit::{OrbitCameraBundle, OrbitCameraController, OrbitCameraPlugin}, LookTransformPlugin};
use std::{fmt::Debug, marker::PhantomData};

#[derive(Clone, Debug, Eq, Hash, PartialEq, SystemSet)]
enum Set {
	UpdatePositions,
	UpdateState,
}

#[tokio::main]
async fn main() {
	let opt = cli::MainOpt::<bc_reader::dup::BcOpt>::parse();

	//let bc_reader = blockchain::mock::BcMock::new(1000);
	let bc_reader = bc_reader::dup::BcDup::new(&opt.bc_opt);
	// let bc_reader = blockchain::substrate::BcSubstrate::new(blockchain::substrate::Config {
	// 	rpc: String::from("ws://127.0.0.1:9944/ws"),
	// })
	// .await;

	Gexplore::run(bc_reader);
}

struct Gexplore<C: 'static + Debug + blockchain::Config, R: 'static + bc_reader::BcReader<C>> {
	_p: PhantomData<(C, R)>,
}

#[allow(clippy::too_many_arguments)]
impl<
		C: 'static + Debug + Send + Sync + blockchain::Config,
		R: 'static + bc_reader::BcReader<C> + Resource,
	> Gexplore<C, R>
{
	fn run(bc_reader: R) {
		App::new()
			.add_event::<state::BlockchainEvent<C>>()
			.init_resource::<offchain::OffchainData<C>>()
			.insert_resource(plugins::bc_reader::BcReaderRes {
				reader: Box::new(bc_reader),
				_p: Default::default(),
			})
			.add_plugins((
				DefaultPlugins,
				AtmospherePlugin,
				LookTransformPlugin,
				OrbitCameraPlugin::default(),
				plugins::wotgraph::WotGraphPlugin::<C>::default(),
				plugins::bc_reader::BcReaderPlugin::<C>::default(),
			))
			.add_systems(Startup, Self::setup)
			.add_systems(Update, Self::keyboard_event_system)
			.configure_sets(Update, Set::UpdatePositions)
			.configure_sets(PreUpdate, Set::UpdateState)
			.run();
	}

	fn setup(mut windows: Query<&mut Window>, mut commands: Commands) {
		windows.single_mut().title = String::from("ğexplore");

		commands.insert_resource(AmbientLight {
			color: Color::WHITE,
			brightness: 0.6,
		});

		commands
			.spawn(OrbitCameraBundle::new(
				OrbitCameraController::default(),
				Vec3::new(-100.0, -100.0, -100.0),
				Vec3::new(0.0, 0.0, 0.0),
				Vec3::Y,
			))
			.insert(Camera3dBundle::default())
			.insert(AtmosphereCamera::default());
	}

	fn keyboard_event_system(
		mut keyboard_input_events: EventReader<KeyboardInput>,
		mut windows: Query<&mut Window>,
		mut bc_reader_cmd_event_writer: EventWriter<plugins::bc_reader::BcReaderCmdEvent>,
	) {
		for event in keyboard_input_events.read() {
			match event {
				KeyboardInput {
					key_code: KeyCode::F11,
					state: ButtonState::Released,
					..
				} => Self::toggle_fullscreen(&mut windows),
				KeyboardInput {
					key_code: KeyCode::NumpadAdd,
					state: ButtonState::Pressed,
					..
				} => {
					bc_reader_cmd_event_writer.send(plugins::bc_reader::BcReaderCmdEvent::ApplyOne);
				}
				KeyboardInput {
					key_code: KeyCode::NumpadSubtract,
					state: ButtonState::Pressed,
					..
				} => {
					bc_reader_cmd_event_writer
						.send(plugins::bc_reader::BcReaderCmdEvent::RevertOne);
				}
				_ => {}
			}
		}
	}

	fn toggle_fullscreen(windows: &mut Query<&mut Window>) {
		let mut window = windows.single_mut();
		window.mode = match window.mode {
			WindowMode::Windowed => WindowMode::Fullscreen,
			_ => WindowMode::Windowed,
		};
	}
}
