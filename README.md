# 3D WoT explorer

(**Early development**) Explore the Web of Trust in time and 3-dimensional space.

(temporary name)

## Run

```bash
sudo sysctl dev.i915.perf_stream_paranoid=0
RUSTFLAGS='-C target-feature=+avx2' cargo run --release

# If you want to use clippy:
RUSTFLAGS='-C target-feature=+avx2' clippy
```

Move camera with `WASD`, apply/revert blocks with numpad's `+-`, free/capture cursor with ESC.

## License

GNU AGPL v3, CopyLeft 2022 Pascal Engélibert

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.  
You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
